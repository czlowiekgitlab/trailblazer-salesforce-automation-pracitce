from abc import ABC

from selenium.common import TimeoutException
from selenium.webdriver import Remote
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class BasePage(ABC):
    """
    Abstract class for page objects
    """
    page_title = ""

    def __init__(self, driver: Remote):  # driver type is specified for IDE code completion
        self.driver = driver

        try:
            WebDriverWait(driver, 10).until(expected_conditions.title_is(self.page_title))
        except TimeoutException:
            raise ValueError(f"Current page {driver.title} is not object page {self.page_title}")


