from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.sales_navbar import SalesNavbar


class SalesHomePage(SalesNavbar):
    page_title = "Home | Salesforce"
