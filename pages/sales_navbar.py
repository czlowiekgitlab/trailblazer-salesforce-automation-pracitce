from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class SalesNavbar(BasePage):
    """
    Parent class for pages with sales navbar
    """
    # Locators
    _accounts_tab_locator = (By.XPATH, "//*[@data-id='Account']")

    # Navigation methods
    def navbar_go_to_sales_accounts_page(self):
        self.driver.find_element(*self._accounts_tab_locator).click()

        from pages.sales_account_list_page import SalesAccountListPage
        return SalesAccountListPage(self.driver)
